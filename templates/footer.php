<footer class="content-info text-center" role="contentinfo">
	<div class="container">
		<?php dynamic_sidebar('sidebar-footer'); ?>
	</div>
</footer>

<?php wp_footer(); ?>
