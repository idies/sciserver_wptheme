<header class="banner navbar navbar-default navbar-static-top" role="banner">
<div class="logo">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-xs-12">
				<a href="/index.php"><img src="/wp-content/uploads/2014/04/sciserver.png" class="sciserverlogo img-responsive" alt="<?php echo get_bloginfo(); ?>"></a>
			</div>
			<div class="col-sm-6 col-xs-12">
				<div class="tagline"><?php echo get_bloginfo ( 'description' );?></div>
			</div>
		</div>
	</div>
</div><!--end of logo -->
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <nav class="collapse navbar-collapse" role="navigation">
      <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav pull-right'));
        endif;
      ?>
    </nav>
  </div>
</header>