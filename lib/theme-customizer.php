<?php
function scis_customizer_sections( $wp_customize ) {

	//Splash Screen
   $wp_customize->add_section( 'scis_splash' , array(
		  'title' => 'Splash',
		  'priority' => 1,
		  'capability' => 'edit_pages',
   ) );
   
   //Splash Title
  $wp_customize->add_setting( 'scis_splash_title' , array(
	  'default' => 'A new vision for science',
	  'sanitize_callback' => 'scis_sanitize_text',
	  'transport' => 'refresh',
	) );

	$wp_customize->add_control( 'scis_splash_title' , array(
		  'label' => 'Splash Title',
		  'section' => 'scis_splash',
		  'type' => 'text',
		  'priority'   => 1
	) );

   //Splash SubTitle
  $wp_customize->add_setting( 'scis_splash_subtitle' , array(
	  'default' => 'SciServer is a collaborative research environment for large-scale data-driven science',
	  'sanitize_callback' => 'scis_sanitize_text',
	  'transport' => 'refresh',
	) );

	$wp_customize->add_control( 'scis_splash_subtitle' , array(
		  'label' => 'Splash Subtitle',
		  'section' => 'scis_splash',
		  'type' => 'text',
		  'priority'   => 2
	) );

   //Splash Background Image
  $wp_customize->add_setting( 'scis_default_image_checkbox', array(
	  'default' => 'true',
	  'transport' => 'refresh'
	) );

	$wp_customize->add_control( new WP_Customize_Control( 
		$wp_customize, 
		'scis_default_image_checkbox', 
		array(
			'label'      => __( 'Use Default Image?', 'sciserver' ),
			'section'    => 'scis_splash',
			'settings'   => 'scis_default_image_checkbox',
			'type' => 'checkbox',
			'priority'   => 3
		)
	));

   //Splash Background Image
  $wp_customize->add_setting( 'scis_splash_image', array(
	  'default' => '/wp-content/themes/sciserver/assets/img/exa_hep_fig2_large.jpg',
	  'transport' => 'refresh'
	) );

	$wp_customize->add_control( new WP_Customize_Image_Control( 
		$wp_customize, 
		'scis_splash_image', 
		array(
			'label'      => __( 'Or choose a Splash Image', 'sciserver' ),
			'section'    => 'scis_splash',
			'settings'   => 'scis_splash_image',
			'priority'   => 4
		)
	));
	
}
add_action( 'customize_register', 'scis_customizer_sections' );
function scis_sanitize_text( $input ) {
    return wp_kses_post( force_balance_tags( $input ) );
}

function scis_customize_css()
{
    if (!get_theme_mod( 'scis_default_image_checkbox' ) ) { 
	?>
         <style type="text/css">
			.intro {background: url('<?php _e( get_theme_mod( 'scis_splash_image' ) ); ?>') 			no-repeat bottom center fixed ;}
         </style>
    <?php
	}
}
add_action( 'wp_head', 'scis_customize_css');

?>