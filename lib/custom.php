<?php
/**
 * Custom functions
 */
add_shortcode('SHOW_COOKIE','show_cookie');

function show_cookie() {
	if (isset($_REQUEST['token'])) return 'Keystone';
	return 'Cookie not found';
}
